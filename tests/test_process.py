from unittest import TestCase, skip
from smtutils.process import Solver, get_msat_path
from smtutils.formula import SmtFormula, Symbol
from smtutils.parsing import SmtResponseParser

class TestProcess(TestCase):
    def test1(self):
        s = Solver(get_msat_path("optimathsat"))
        f = SmtFormula()
        a = Symbol("Int", "a")
        b = Symbol("Int", "b")
        f.assert_(a+b == 5)
        f.assert_(a < 100)
        #f.minimize(b)
        f.check_sat()
        f.get_values(a, b)
        print(str(f))
        res =  s.run_formula(f)
        p = SmtResponseParser(res)
        print(p.result, p.model)

    @skip
    def test2(self):
        s = Solver(get_msat_path("optimathsat"))
        f = SmtFormula()
        a = Symbol("Int", "a")
        b = Symbol("Int", "b")
        f.assert_(a+b == 5)
        f.assert_(a < 100)
        s.load_formula(str(f))
        s.load_formula("(check-sat)")
        print(s.process.stdout.readable())
        for line in (s.get_result()):
            print(line)
