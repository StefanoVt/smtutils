from unittest import TestCase
from smtutils import formula

class SmtUtilsTest(TestCase):
    def test1(self):
        a = formula.SmtFormula()
        self.assert_(str(a) == "")

    def test2(self):
        a = formula.Symbol("Int", "a")
        b = formula.Symbol("Int", "b")
        self.assertEqual(str(a), "a")
        self.assertEqual(str(b), "b")
        self.assertEqual(str(a+b), "(+ a b)")
        self.assertEqual(str(a+b+b), "(+ a b b)")
        self.assertEqual(str(~a), "(not a)")

    def test3(self):
        f = formula.SmtFormula()
        a = formula.Symbol("Int", "a")
        b = formula.Symbol(("Int", "(Int)"), "b")
        f.minimize(a)
        self.assert_("a" in f.declared)
        self.assert_("b" not in f.declared)
        f.assert_(a + b(3) == 0)
        self.assert_("b" in f.declared)

    def test_negatives(self):
        a = formula.Symbol("Int", "a")
        self.assertEqual(str(a == -1), "(= a (- 1))")
        self.assertEqual(str(a >= -1), "(>= a (- 1))")