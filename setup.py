#!/usr/bin/env python
from setuptools import find_packages, setup

setup(
    name='smtutils',
    packages=find_packages(exclude=['tests']),
    install_requires=['sexpdata'],
    test_suite='tests', )