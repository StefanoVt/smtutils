
import subprocess
from distutils.spawn import find_executable
from six.moves.configparser import ConfigParser
from typing import AnyStr, Iterable, Iterator, Any

def get_msat_path(name):
    # type: (str) -> str
    "old, replace with path finding code"
    path = find_executable(name)
    if not path:
        conf = ConfigParser()
        conf.read(".insert_solver_path_here.txt")
        path = conf.get("optimathsat", "path")
    return path

class Solver(object):
    "Class representing SMT solver processes"
    def __init__(self, path):
        # type: (str) -> None
        "Initialize process: path is the executable path"
        self.binpath = path
        self.process = subprocess.Popen(self.binpath, stdin=subprocess.PIPE, stdout=subprocess.PIPE)

    def load_formula(self, formula):
        # type: (AnyStr) -> None
        " xxx deadlock do not use"
        self.process.stdin.write(str(formula).encode())
        self.process.stdin.write(b"\n")
        self.process.stdin.flush()

    def get_result(self):
        # type: () -> Iterator[str]
        while not self.process.stdout.closed and self.process.poll() is None:
            yield self.process.stdout.readline().decode()

    def run_formula(self, formula):
        # type: (Any) -> str
        "Run a formula with the solver, not interactive"
        return (self.process.communicate(str(formula).encode())[0]).decode()